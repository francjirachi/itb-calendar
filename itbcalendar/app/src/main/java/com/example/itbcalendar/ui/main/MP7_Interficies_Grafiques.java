package com.example.itbcalendar.ui.main;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.itbcalendar.R;

public class MP7_Interficies_Grafiques extends Fragment {

    private Mp7InterficiesGrafiquesViewModel mViewModel;

    public static MP7_Interficies_Grafiques newInstance() {
        return new MP7_Interficies_Grafiques();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mp7__interficies__grafiques_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(Mp7InterficiesGrafiquesViewModel.class);
        // TODO: Use the ViewModel
    }

}
